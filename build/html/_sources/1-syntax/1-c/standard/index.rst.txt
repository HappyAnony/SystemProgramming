语法规范
=========



linux风格
~~~~~~~~~~~

变量定义

- \ ``int dog_name``\ 



windows风格
~~~~~~~~~~~~
变量定义

- \ ``int DogName``\ 


编程习惯
~~~~~~~~~~~

- 定义全局变量：以\ ``g``\ 开头
- 定义结构体：以\ ``_t``\ 结尾
