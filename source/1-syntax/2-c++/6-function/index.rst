函数
=================

C++中所有的函数都必须有类型，C语言中的默认类型在C++中是不合法的，C++更加强调类型，任意的程序元素都必须显示指明类型

- 在C语言中

	- \ ``int f()``\ ：表示返回值为\ ``int``\ ，接受任意参数的函数
	- \ ``int f(void)``\ ：表示返回值为\ ``int``\ 的无参函数
	- 实参的个数可以大于或等于形参的个数
- 在C++语言中

	- \ ``int f()``\ ：表示返回值为\ ``int``\ 的无参函数
	- \ ``int f(void)``\ ：表示返回值为\ ``int``\ 的无参函数
	- 实参的个数必须等于形参的个数
