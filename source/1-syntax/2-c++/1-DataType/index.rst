数据类型
=================

C++对C语言数据类型特性的增强

- \ `enum枚举类型 <#enumll>`_\ 

C++新增数据类型

- \ `bool布尔类型 <#booll>`_\ 





.. _enumll:

enum枚举类型
~~~~~~~~~~~~~~

在c语言中枚举类型的本质就是整型，枚举变量可以用任意整型赋值；而在c++中枚举变量, 只能用被枚举出来的元素初始化

.. code-block:: c++

	#include <iostream>
	using namespace std;
	enum season
	{
		SPR,
		SUM,
		AUT,
		WIN
	};

	int main()
	{
	    enum season s = SPR;
		/*此处使用非枚举元素初始化变量，会报语法错误，但是C语言可以通过*/
	    //s = 0;
	    s = SUM;
	    cout << "s = " << s << endl;
	    return 0;
	}


.. _booll:

bool类型
~~~~~~~~~~~

C++在C语言的基本类型上新增\ ``bool``\ 数据类型，C++中的\ ``bool``\ 可取的值只有

- \ ``true``\ ：代表真值，编译器内部用\ ``1``\ 来表示
- \ ``false``\ ：代表非真值，编译器内部用\ ``0``\ 来表示 

理论上\ ``bool``\ 只占用一个字节，如果多个\ ``bool``\ 变量定义在一起，可能会各占一个\ ``bit``\ ，这取决于编译器的实现；C++编译器会在赋值时将\ ``非0值``\ 转换为\ ``true``\ ，\ ``0值``\ 转换为\ ``false``\ 

.. code-block:: c++

	#include <iostream>

	using namespace std;
	int main(int argc,  char *argv[])
	{
	    int a;
	    bool b = true;
	    printf("b = %d, sizeof(b) = %d\n", b, sizeof(b));
	    b = 4;
	    a = b;
	    printf("a = %d, b = %d\n", a, b);
	    b = ‐4;
	    a = b;
	    printf("a = %d, b = %d\n", a, b);
	    a = 10;
	    b = a;
	    printf("a = %d, b = %d\n", a, b);
	    b = 0;
	    printf("b = %d\n", b);
	    return 0;
	}


