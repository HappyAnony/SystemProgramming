运算符
=================


三目运算符

- 在C语言中的三目运算符返回的是变量值，不能作为左值使用
- 在C++中的三目运算符可直接返回变量本身，因此可以出现在程序的任何地方；但是也有一个限制条件，那就是三目运算符可能返回的值中如果有一个是常量值，也不能作为左值，例如：\ ``(a < b ? 1 : b ) = 30;``\ 

.. code-block:: c++

	#include <iostream>
	using namespace std;
	int main(void)
	{
	    int a = 10;
	    int b = 20;

	    /*三目运算符是一个表达式，表达式不可能做左值，它将返回一个最小数变量，并且给这个变量赋值成30*/
	    (a < b ? a : b) = 30;
	    printf("a = %d, b = %d\n", a, b);
	    return 0;
	}