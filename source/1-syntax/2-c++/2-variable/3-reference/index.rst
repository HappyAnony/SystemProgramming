引用
=========================

- \ `引用本质 <#refernature>`_\ 
- \ `实现原理 <#referprinl>`_\ 
- \ `引用规则 <#referrule>`_\ 
- \ `引用类型 <#refertype>`_\ 
- \ `引用应用 <#referappply>`_\ 



.. _refernature:

0x00 引用本质
~~~~~~~~~~~~~~~~~~~~~~~~~

在C中，通过数据类型来申请内存空间，通过变量名来引用内存空间的数据，变量名的本质就是一段连续内存空间的别名

那么问题就来了，对一段连续的内存空间还可以取其它别名吗?

在C++中，对一个已定义变量(已知连续内存空间)取的别名叫做\ **引用**\ ，即\ **引用的本质**\ 就是：一段连续内存空间的除变量名之外的其它别名

声明\ **引用**\ 的语法是：\ ``DateType &ReferName = VarName;``\ 

.. code-block:: c++

	#include <iostream>

	struct Teacher
	{
		int &a;
		int &b;
	};

	using namespace std;

	int main(void)
	{
		/*编译器分配4个字节内存, a为内存空间的别名*/
	    int a = 10;
		/*将b定义为a的别名;单独定义的引用时，必须初始化*/
	    int &b = a;
	    a = 11;

	    {
	        int *p = &a;
	        *p = 12;
	        cout << a <<endl;
	    }

	    b = 14;
	    cout << "a = " << a << ", b = " << b <<endl;
		cout << "sizeof(Teacher) : " << sizeof(Teacher) << endl;
	    return 0;
	}

引用是作为其它变量的别名而存在，因此在一些场合可以代替指针，引用相对于指针来说具有更好的可读性和实用性，因此在C++中可以用引用解决的问题。避免用指针来解决

.. code-block:: c++

	/*无法实现两数据的交换*/
	void swap(int a, int b);

	/*通过指针来实现数据的交换 */
	void swap(int *p, int *q);

	/*通过引用来实现数据的交换*/
	void swap(int &a, int &b)
	{
	    int tmp;
	    tmp = a;
		a = b;
	    b = tmp;
	}

	int main()
	{
	    int a = 3, b = 5;
	    cout << "a = " << a << "b = " << b << endl;
	    swap(a, b);
	    cout << "a = " << a << "b = " << b << endl;
	    return 0;
	}


.. _referprinl:

0x01 实现原理
~~~~~~~~~~~~~~~~~~

引用在C++中的内部实现是一个常指针：\ ``DataType& ReferName <===> DataType * const ReferName``\ 

C++编译器在编译过程中使用常指针作为引用的内部实现，因此引用所占用的空间大小与指针相同；从使用的角度，引用会让人误会其只是一个别名，没有自己的存储空间；这是C++为了实用性而做出的细节隐藏

引用可以代替指针实现间接赋值，我们知道间接赋值的必要条件是：实参传入地址，形参使用指针变量接收地址，通过形参指针变量间接修改实参值

引用实现间接赋值的原理有些许不同：实参传入变量名即可，不需要传入变量地址，调用函数时，c++编译器会自动取实参变量地址赋值给形参引用(常量指针)，通过形参引用(常量指针)来间接修改实参的值

.. code-block:: c++

	/*&a = &x*/
	void funcRefer(int &a)
	{
	    a = 5;
	}

	/*a = &x*/
	void funcPointer(int *const a)
	{
	    *a = 6;
	}

	int main()
	{
	    int x  = 10;
		funcRefer(x);    /*实参为变量名，编译器会自动取变量地址*/
		funcPointer(&x); /*实参为变量地址，需手动取变量地址*/
	    return 0;
	}




.. _referrule:

0x02 引用规则
~~~~~~~~~~~~~~~~~~~~~~~~~

引用的规则有

- 引用没有定义，是一种关系型声明，声明它和原有某一变量(实体)的关系，故而类型与原类型保持一致，与被引用的变量有相同的地址空间
- 声明的时候必须初始化，一经声明，不可变更(做函数参数时可不用初始化)
- 可对引用，再次引用，多次引用的结果，是某一变量具有多个别名
- \ ``&``\ 符号前有数据类型时，是引用，其它皆为取地址

.. code-block:: c++

	int main(void)
	{
	    int a, b;
	    int &r = a;

		/*错误，不可更改原有的引用关系*/
	    int &r = b;

		/*错误，引用类型不匹配*/
	    float &rr = b;

		/*可对引用再次引用,表示a变量有两个别名，分别是r和ra*/
	    int &ra = r;

		/*变量与引用具有相同的地址*/
		cout << &r << &a << &ra << endl;
	    return 0;
	} 


.. _refertype:

0x03 引用类型
~~~~~~~~~~~~~~~~~~~~~~~~~

常用引用类型有

- \ `指针引用 <#pointrefer>`_\ 
- \ `const引用 <#constrefer>`_\ 


.. _pointrefer:

0x0300 指针引用
~~~~~~~~~~~~~~~~

.. code-block:: c++

	#include <iostream>
	using namespace std;

	struct Teacher
	{
	    char name[64];
	    int age ;
	};


	/*在被调用函数获取资源*/
	int getTeacher(Teacher **p)
	{
	    Teacher *tmp = NULL;
	    if (p == NULL)
	    {
	        return ‐1;
	    }

	    tmp = (Teacher  *)malloc(sizeof(Teacher));
	    if (tmp == NULL)
	    {
	        return ‐2;
	    }
	    tmp‐>age = 33;
	    
	    *p = tmp;
	    return 0;
	}

	/*指针的引用做函数参数*/
	int getTeacher2(Teacher* &myp) 
	{
	    /*给myp赋值相当于给main函数中的pT1赋值*/
	    myp = (Teacher  *)malloc(sizeof(Teacher));
	    if (myp == NULL)
	    {
	        return  ‐1;
	    }

	    myp->age =36;
	    return 0;
	}

	void FreeTeacher(Teacher  *pT1)
	{
	    if (pT1 == NULL)
	    {
	        return ;
	    }

	    free(pT1);
	}

	int main(void)
	{
	    Teacher *pT1 = NULL;
	    
		/*c语言中的二级指针*/
	    getTeacher(&pT1);
	    cout << "age: " << pT1‐>age << endl;
	    FreeTeacher(pT1);

	    /*c++中的引用(指针的引用)*/
	    getTeacher2(pT1);
	    cout << "age: " << pT1‐>age << endl;
	    FreeTeacher(pT1);
	    return 0;
	}

.. _constrefer:

0x0301 const引用
~~~~~~~~~~~~~~~~~~~~

const引用有较多使用；它可以防止对象的值被随意修改，因而具有一些特性

- const对象的引用必须是const的，将普通引用绑定到const对象是不合法的，这个原因比较简单，既然对象是const的，表示不能被修改，引用当然也不能修改，必须使用const引用，实际上\ ``const int a = 1; int &b = a;``\ 这种写法是不合法的编译不过
- const引用可使用相关类型的对象(常量,非同类型的变量或表达式)初始化，这个是const引用与普通引用最大的区别，\ ``const int &a=2;``\ 是合法的；\ ``double x = 3.14; const int &b = a;``\ 也是合法的

.. code-block:: c++

	#include <iostream>
	using namespace std;

	int main(void)
	{
	    /*普通引用*/
	    int a = 10;
	    int &b = a;
	    cout << "b = " << b << endl;

	    /*常引用*/
	    int x = 20;
		/*常引用是限制变量为只读不能通过y去修改x了*/
	    const int &y = x;
		/*语法错误*/
	    //y = 21;
	    return 0;
	}


const引用的目的是：禁止通过修改引用值来改变被引用的对象；const引用的初始化特性较为微妙，可通过如下代码说明

.. code-block:: c++

	double val = 3.14;
	const int &ref = val;
	double & ref2 = val;
	cout << ref << " " << ref2 << endl;
	val = 4.14;
	cout << ref << " " << ref2 << endl;

上述输出结果为\ ``3 3.14``\ 和\ ``3 4.14``\ ；因为\ ``ref``\ 是\ ``const``\ 的，在初始化的过程中已经给定值，不允许修改，而被引用的对象是\ ``val``\ ，是非\ ``const``\ 的，所以\ ``val``\ 的修改并未影响\ ``ref``\ 的值，而\ ``ref2``\ 的值发生了相应的改变

那么，为什么非\ ``const``\ 的引用不能使用相关类型初始化呢？实际上\ ``const``\ 引用使用相关类型对象初始化时发生了如下过程 

.. code-block:: c++

	int temp = val;
	const int &ref = temp;

如果\ ``ref``\ 不是\ ``const``\ 的，那么改变\ ``ref``\ 值，修改的是\ ``temp``\ ，而不是\ ``val``\ ；期望对\ ``ref``\ 的赋值会修改\ ``val``\ ，而\ ``val``\ 实际并未修改

.. code-block:: c++

	#include <iostream>
	using namespace std;
	int main(void)
	{
	    /*用变量初始化常引用*/
	    int x1 = 30;
		/*用x1变量去初始化常引用*/
	    const int &y1 = x1;

	    /*用字面量初始化常量引用*/
	    const int a = 40;   //c++编译器把a放在符号表中
		/*语法错误，普通引用引用一个字面量，字面量没有内存地址*/
	    //int &m = 41;
		/*c++编译器会分配内存空间
		 *int temp = 43
		 *const int &m = temp;*/
	    const int &m = 43;
	    return 0;
	}


.. code-block:: c++

	#include <iostream>
	using namespace std;

	struct Teacher
	{
	    char name[64];
	    int age;
	};

	void printTeacher(const Teacher &myt)
	{
	    /*常引用让实参变量拥有只读属性*/
	    //myt.age = 33;
	    printf("myt.age:%d\n", myt.age);
	}

	int main(void)
	{
	    Teacher t1;
	    t1.age = 36;
	    printTeacher(t1);
	    return 0;
	}

终上所述

- \ ``const int &e``\ 相当于\ ``const int * const e``\ 
- 普通引用相当于\ ``int *const e``\ 
- 当使用常量(字面量)对const引用进行初始化时，C++编译器会为常量值分配空间，并将引用名作为这段空间的别名
- 使用字面量对const引用初始化后，将生成一个只读变量


.. _referappply:

0x04 引用应用
~~~~~~~~~~~~~~~~~~~~~~~~~

引用的应用有

- \ `引用作为函数参数 <#referparam>`_\ 
- \ `引用作为函数返回值 <#referreturn>`_\ 


.. _referparam:

0x0400 引用作为函数参数
~~~~~~~~~~~~~~~~~~~~~~~~~

普通引用在声明时必须用其它的变量进行初始化，引用作为函数参数声明时可以不进行初始化

.. code-block:: c++

	#include <iostream>

	using namespace std;

	struct Teacher
	{
	    char name[64];
	    int age;
	};

	void printfT(Teacher *pT)
	{
	    cout << pT‐>age << endl;
	}

	/*形参pT是实参t1的别名，pT类似于指针变量指向t1的内存空间*/
	void printfT2(Teacher &pT)
	{
	    pT.age = 33;
	    cout << pT.age << endl;
	}

	/*pT和t1是两个不同的变量，pT是t1的一份数据拷贝*/
	void printfT3(Teacher pT)
	{
	    cout << pT.age << endl;

		/*只会修改形参pT变量，不会修改实参t1变量*/
	    pT.age = 45;
	}

	int main(void)
	{ 
	    Teacher t1;
	    t1.age = 35;
	    printfT(&t1);
	    printfT2(t1);
	    printf("t1.age:%d\n", t1.age); //输出结果为33
	    printfT3(t1);
	    printf("t1.age:%d\n", t1.age); //输出结果为33
	    return 0;
	}

.. _referreturn:

0x0401 引用作为函数返回值
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

当函数返回值为引用时，若返回栈变量，不能成为其它引用的初始值(不能作为左值使用)

.. code-block:: c++

	include <iostream>
	using namespace std;
	int getA1()
	{
	    int a;
	    a = 10;
	    return a;
	}

	int& getA2()
	{
	    int a;
	    a = 10;
	    return a;
	}

	int main(void)
	{
	    int a1 = 0;
	    int a2 = 0;
	    
		/*简单的值拷贝*/
	    a1 = getA1();

	    /*将一个引用赋给一个变量，会有拷贝动作；编译器类似做了如下隐藏操作a2 = *(getA2())*/
	    a2 = getA2();

	    /*将一个引用赋给另一个引用作为初始值，由于是栈的引用，内存非法*/
	    int &a3 = getA2();
	    cout << "a1 = " << a1 <<endl;
	    cout << "a2 = " << a2 <<endl;
	    cout << "a3 = " << a3 <<endl;
	    return 0;
	}


当函数返回值为引用时, 若返回静态变量或全局变量，可以成为其他引用的初始值(可作为右值使用，也可作为左值使用)

.. code-block:: c++

	#include <iostream>
	using namespace std;

	int getA1()
	{
	    static int a;
	    a = 10;
	    return a;
	}

	int& getA2()
	{
	    static int a;
	    a = 10;
	    return a;
	}

	int main(void)
	{
	    int a1 = 0;
	    int a2 = 0;

	    /*值拷贝*/
	    a1 = getA1();

	    /*将一个引用赋给一个变量，会有拷贝动作，编译器类似做了如下隐藏操作a2 = *(getA2())*/
	    a2 = getA2();

	    /*将一个引用赋给另一个引用作为初始值，由于是静态区域，内存合法*/
	    int &a3 = getA2();
	    cout << "a1 = " << a1 << endl;
	    cout << "a2 = " << a2 << endl;
	    cout << "a3 = " << a3 << endl;
	    return 0;
	} 


引用作为函数返回值，如果返回值为引用可以当左值，如果返回值为普通变量不可以当左值

.. code-block:: c++

	#include <iostream>
	using namespace std;
 
	/*返回变量的值*/
	int func1()
	{
	    static int a1 = 10;
	    return a1;
	}

	/*返回变量本身*/
	int& func2()
	{
	    static int a2 = 10;
	    return a2;
	}

	int main(void)
	{
	    /*函数当右值*/
	    int c1 = func1();
	    cout << "c1 = " << c1 << endl;

		/*函数返回值是一个引用，并且当右值*/
	    int c2 = func2();
	    cout << "c2 = " << c2 << endl;

	    /*函数当左值，语法错误*/
	    //func1() = 100;

		/*函数返回值是一个引用,并且当左值，语法正确*/
		func2() = 100;

	    c2 = func2();
	    cout << "c2 = " << c2 << endl;
	    return 0;
	} 