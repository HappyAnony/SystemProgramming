变量
=========================


在c语言中，变量必须在引用前就完成声明定义，否则无法进行调用；但是在C++中变量可以做到随用随定义，即在引用的同时可以对变量进行声明和定义

.. code-block:: c++

	#include  <iostream> 

	using  namespace  std; 
	//C语言中的变量都必须在作用域开始的位置定义
	//C++中更强调语言的实用性，所有的变量都可以在需要使用时再定义
	int  main(void) 
	{
		/*此处变量i的作用域为for循环体*/
		for (int i = 0; i < 4; i++)
		{
			cout << "i = " << i <<endl; 
		}
	    return  0; 
	}


在C语言中，重复定义多个同名的全局变量是合法的，多个同名的全局变量最终会被链接到全局数据区的同一个地址空间上；在C++中，不允许定义多个同名的全局变量

.. code-block:: c++

	#include  <iostream> 
	int g_var;
	/*重复定义全局变量存在语法错误*/
	// int g_var = 1;
	int main(int argc,  char *argv[]) 
	{ 
	    printf("g_var  =  %d\n",  g_var); 
	    return  0; 
	}


在C语言中，\ ``struct``\ 定义了一组变量的集合，C编译器并不认为这是一种新的类型；但在C++中，\ ``struct``\ 是一个新类型的定义声明

.. code-block:: c++

	#include <iostream>
	struct Student
	{
	    char name[100];
	    int age;
	};

	int main(int argc,  char *argv[])
	{
	    Student  s1  =  {"wang",  1};
	    Student  s2  =  {"wang2",  2};
	    return  0;
	}
