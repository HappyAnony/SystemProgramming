常量与变量
=================

C++对C语言常量和变量特性的增强

.. toctree::
   :titlesonly:
   :glob:

   1-const/index
   2-variable/index

C++对C语言变量的拓展

.. toctree::
   :titlesonly:
   :glob:

   3-reference/index
