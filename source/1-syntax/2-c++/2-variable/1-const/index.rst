常量
=========================


在C中定义常量有两种方法

- \ ``const``\ ：在编译期间分配空间，由编译器处理的，提供类型检查和作用域检查
- \ ``define``\ ：在预编译期间展开，由预处理器处理，单纯的文本替换

.. code-block:: c++


	#include <iostream>

	/*定义一个字符串常量宏，在预编译时候展开*/
	#define NAME  "anony"
	int main(void)
	{
		/*都是定义了一个常整型数*/
	    const  int  a; 
	    int  const  b; 
	    
		/*定义了一个指向常整形数的指针(该指针所指向的内存数据不能被修改，但是该指针可以指向其它内存地址)*/
	    const int *c;

		/*定义了一个常指针(该指针指向的内存地址不能被修改，但指向的内存数据可以被修改)*/
	    int * const d;

		/*定义了一个指向常整形的常指针(该指针指向的内存地址以及内存数据都不能被修改)*/
	    const int * const e;
	    return  0; 
	}

但是在C中\ ``const``\ 数据类型定义的常量严格意义上说是一个变量，因为它的值依然可以被强制修改

.. code-block:: c++

	#include <stdio.h>

	int main()
	{
		/*此时a是一个伪常量*/
	    const int a = 10;

		/*将const常量a的地址赋值给指针变量p*/
	    int *p = (int*)&a;

	    printf("a = %d\n", a);  //结果为10
	    *p = 11;
	    printf("a = %d\n", a);  //结果为11
	    return  0;
	}


而C++中\ ``const``\ 修饰的是一个真正的常量，其值是不能被修改，可以将其理解为宏定义；const修饰的常量在编译期间，就已经确定下来了


.. code-block:: c++

	#include <iostream>

	using namespace std;
	int main()
	{
		/*此时a是一个常量，以a为key，10为value存放在text段中的符号表中*/
	    const int a = 10;

		/*a是一个常量，对常量取地址时，编译器会临时开辟一段内存空间，将这个空间地址赋值为指针变量*/
	    int *p = (int*)&a;

	    cout << "a = " << a << endl;  //结果为10
	    *p = 11;
	    cout << "a = " << a << endl;  //结果为10
	    return  0;
	}


终上所述

- C语言中的const常量是一个可修改值的伪常量(即变量)，有自己的存储空间
- C++语言中的const常量是一个不可修改值的真常量，它存放在text段的符号表中

	- 可能分配存储空间，也可能不分配存储空间
	- 当const常量为全局，并且需要在其它文件中使用，会分配存储空间
	- 当使用\ ``&``\ 操作符，取const常量的地址时，会分配存储空间
	- 当const修饰引用时(例如\ ``const int &a = 10;``\ )，也会分配存储空间