c语言篇
========

参考文档

- \ `Linux系统调用列表 <https://www.ibm.com/developerworks/cn/linux/kernel/syscall/part1/appendix.html#2>`_\ 
- \ `linux系统调用接口整理 <http://blog.sina.com.cn/s/blog_703f58b101011qan.html>`_\ 
- \ `linux系统调用表 <https://blog.csdn.net/sinat_26227857/article/details/44244433>`_\ 
- \ `Linux系统调用列表1 <https://blog.csdn.net/wwwdc1012/article/details/78759326>`_\ 
- \ `系统调用和系统API <https://www.cnblogs.com/happyliuyi/p/5126876.html>`_\ 
- \ `c标准库和操作系统api的关系是怎样的 <https://www.zhihu.com/question/46763480>`_\ 

.. figure:: images/1.jpg

.. figure:: images/2.jpg

目录
------

.. toctree::
   :titlesonly:
   :glob:


   1-LibraryC/index
   2-SysCall/index
   3-UserDefine/index
